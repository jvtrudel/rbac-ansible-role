# RBAC - Rôle ansible

Role ansible pour la gestion de l'authentification et de l'autorisation d'utilisateurs linux basée sur des roles


## Principes du design

  - La configuration a 2 niveau:
    - définition des rôles 
    - autorisations et description des utilisateurs
  - Les informations sur les utilisateurs est récupéré de sources publiques


## Rôles implémentés

  - god


# Usage

    - role:
        name: rbac-ansible-role
      vars:
        username: jvtrudel
        rbac: god
        ssh_pubkey_provider: gitlab # github

## todo: implémenter une liste d'utilisateurs et une liste de roles

    - role:
        name: rbac-ansible-role
      vars:
        users:
          - username: jvtrudel
            ssh_public_key_provider: gitlab
            roles:
              - god
